<h1 align="center">                     SIM - 2021</h1>
<h1 align="center">          SISTEMA INTEGRADO DE MONITORAMENTO</h1>
<h1></h1>

<p>O objetivo deste projeto monitorar e otimizar o funcionamento de uma estufa, tornando a o mais autonoma possível. </p>
<p>Buscar eficiência no processo da coleta de dados e tomada de decisões com intervençãornar minima do usuário.</p>
<p>Evitar o disperdicio de recursos naturais, energeticos e tornar o processo mais eficiente. </p>
<p></p>

Conheca os **Membros da equipe** no final da página.


- [x] <h1 align="center">1.0 ENTREGA 01 (02/03/2021)</h1>
- [X] <h1 align="center">2.0 ENTREGA 02 (30/03/2021)</h1>
- [x] <h1 align="center">3.0 ENTREGA 04 (08/04/2021)</h1>
- [ ] <h1 align="center">4.0 ENTREGA 05 (20/04/2021)</h1>
- [ ] <h1 align="center">5.0 ENTREGA 06 (04/05/2021)</h1>
- [ ] <h1 align="center">6.0 RELATÓRIO FINAL (03/06/2021)</h1>
- [ ] <h1 align="center">7.0 APRESENTAÇÃO DO PROJETO (10/06/2021)</h1>



<b>CRONOGRAMA COMPLETO E DETALHADO, clique [aqui](https://gitlab.com/bigdata.fatec/sim/-/wikis/CRONOGRAMA).


TIME
- [Marcos Albuquerque Macedo [S.M]](https://gitlab.com/BDAg/dadosnematologicos2019/wikis/Marcos-Albuquerque-Macedo)
- [Drielli Laurine Meireles Cândido [P.O.]](https://gitlab.com/BDAg/nemasystem/wikis/Drielli)
- [Maria Eduarda de Lima Rosa](https://drive.google.com/file/d/1oCq65F-QBmzDi6CcGgnQKX8ezEfmYjdU/view?usp=sharing)
- [Lucas Natan Camacho da Silva](https://drive.google.com/file/d/1qFCVvBqgnnAWMfvqavL-0_qua2slksYc/view?usp=sharing)
- [José Eduardo Gonçalves Maran](https://drive.google.com/file/d/1k7ZprR0U92RN0YO6UhK91AQyyiMabf2J/view?usp=sharing) 
- [Nathan da Silva Santos]()



